create database genai;
use genai;

CREATE TABLE Pokemon (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    primary_type VARCHAR(50) NOT NULL,
    secondary_type VARCHAR(50)
);

CREATE TABLE Move (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    power INT NOT NULL,
    type VARCHAR(50) NOT NULL
);

CREATE TABLE Pokemon_Move (
    pokemon_id INT,
    move_id INT,
    PRIMARY KEY (pokemon_id, move_id),
    FOREIGN KEY (pokemon_id) REFERENCES Pokemon(id),
    FOREIGN KEY (move_id) REFERENCES Move(id)
);

INSERT INTO Pokemon (name, primary_type, secondary_type) VALUES
    ('Bulbasaur', 'Grass', NULL),
    ('Charmander', 'Fire', NULL),
    ('Squirtle', 'Water', NULL),
    ('Eevee', 'Normal', NULL),
    ('Pidgey', 'Normal', 'Flying');


INSERT INTO Move (name, power, type) VALUES
    ('Tackle', 35, 'Normal'),
    ('Water Gun', 40, 'Water'),
    ('Ember', 40, 'Fire'),
    ('Vine Whip', 40, 'Grass'),
    ('Wing Attack', 65, 'Flying'),
    ('Headbutt', 70, 'Normal'),
    ('Return', 100, 'Normal');


INSERT INTO Pokemon_Move (pokemon_id, move_id) VALUES
    (1, 1), 
    (1, 4),
    (1, 7);


INSERT INTO Pokemon_Move (pokemon_id, move_id) VALUES
    (2, 1), 
    (2, 3),
    (2, 7);


INSERT INTO Pokemon_Move (pokemon_id, move_id) VALUES
    (3, 1), 
    (3, 2), 
    (3, 7);


INSERT INTO Pokemon_Move (pokemon_id, move_id) VALUES
    (4, 1), 
    (4, 6), 
    (4, 7);


INSERT INTO Pokemon_Move (pokemon_id, move_id) VALUES
    (5, 1), 
    (5, 5),
    (5, 7);

SELECT p.name
FROM Pokemon p
JOIN Pokemon_Move pm ON p.id = pm.pokemon_id
JOIN Move m ON pm.move_id = m.id
WHERE m.name = 'Return';

SELECT name
FROM Move
WHERE type IN ('Fire', 'Flying');
